<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Template de front</title>
	<meta name="description" content="description de la page de front">
	<link rel="stylesheet" type="text/css" href="css/_style.css">
</head>
<body>
<header>
			<div class="container">
				<a href="#" id="logo">
					<img src="images/logo.svg" alt="">
				</a>
				<nav id="main-nav">
					<ul>
						<li><a href="#">Accueil</a></li>
						<li><a href="#">Utilisateurs</a></li>
						<li><a href="#">Utilitaires</a></li>
						<li><a>|</a></li>
						<li><a href="#">Personnalisé</a></li>
						<li><a href="#">Se Connecter</a></li>
					</ul>
				</nav>
			</div>
		</header>
		<main>
			<section id="section1">
				<!-- afficher la vue -->
				<?php include $this->view ?>
			</section>
		</main>
		



</body>
</html>