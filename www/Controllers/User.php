<?php

namespace App\Controller;

use App\Core\View;
use App\Core\FormValidator;
use App\Models\User as UserModel;
use App\Models\Page;

class User
{

	//Method : Action
	public function defaultAction(){
		echo "User default";
	}

	//Method : Action
	public function registerAction(){
		
		//Récupérer le formulaire
		//Récupérer les valeurs de l'internaute si il y a validation du formulaire
		//Vérification des champs (uncitié de l'email, complexité du pwd, ...)
		//Affichage du résultat
		/*$view = new View("register");
		$user = new User();
		$form = $this-createForm(RegisterType::class);
		$form->handle();*/

		$user = new userModel();
		$view = new View("register");

		$form = $user->formbuilderRegister();

		if(!empty($_POST)){

			$errors = FormValidator::check($form, $_POST);

			if(empty($errors)){
				$user->setPseudo($_POST["pseudo"]);
				$user->setEmail($_POST["email"]);
				$user->setPwd($_POST["pwd"]);

				$user->save();
			}else{
				$view->assign("errors", $errors);
			}
		}

		$view->assign("form", $form);
		$view->assign("formLogin", $user->formBuilderLogin());


	}

	public function loginAction(){
		$view = new View("login");
	}

	//Method : Action
	public function showAction(){
		
		//Affiche la vue user intégrée dans le template du front
		$view = new View("user"); 
	}



	//Method : Action
	public function showAllAction(){
		
		//Affiche la vue users intégrée dans le template du back
		$view = new View("users", "back"); 
		
	}
	
}
