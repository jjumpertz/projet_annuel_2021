<?php

namespace App\Core;

use MongoDB\Driver\Exception\LogicException;

class Manager
{
    private $table;
    private $connection;
    private $class;

    public function __construct(string $class, string $table, BDDInterface $connection = null)
    {
        $this->class = $class;
        $this->table = $table;

        $this->connection = $connection;
        if (NULL === $connection)
            $this->connection = new PDOConnection();
    }

    public function save($objectToSave)
    {
        $objectArray = $objectToSave->__toArray();
        $columnsData = array_values($objectArray);
        $columns = array_keys($objectArray);
        $params = [];

        foreach ($objectArray as $key => $value) {
            if ($value instanceof Model) {
                $params[":$key"] = $value->getId();
            } else {
                $params[":$key"] = $value;
            }
        }

        if (!is_numeric($objectToSave->getId())) {
            array_shift($columns);
            array_shift($params);
            //INSERT
            $sql = $this->pdo->prepare("INSERT INTO" . $this->table . "(" . implode(',', array_keys($columns)) . ")
            VALUES
            (:" . implode(',:', array_keys($columns)) . ") ");
        } else {
            //UPDATE
            foreach ($columns as $column) {
                $sqlUpdate[] = $column . "=:" . $column;
            }
            $sql = "UPDATE " . $this->table . "SET " . implode(",", $sqlUpdate) . "WHERE id=:id;";
        }
        error_log('SQL : Save : requete => ' . $sql);
        error_log('SQL : Save : params =>' . print_r($params, true));
        $this->connection->query($sql, $params);
    }

    public function find(int $id): ?Model
    {
        $sql = "SELECT * FROM $this->table where id = :id";
        error_log('SQL : Find : requete => ' . $sql);
        $result = $this->connection->query($sql, [':id' => $id]);
        return $result->getOneOrNullResult($this->class);
    }




    //fonction qui renvoi un tableau avec toutes les valeurs
    public function findAll(): array
    {
        $sql = "SELECT * FROM $this->table";
        error_log('SQL : FindAll : requete => '.$sql);
        $result = $this->connection->query($sql);
        return $result->getArrayResult($this->class);
    }

    public function delete (int $id): bool
    {
        $sql = "DELETE FROM $this->table where id = :id";
        error_log('SQL : delete : requete => '.$sql);
        $result = $this->connection->query($sql, [':id' => $id]);

        return true;
    }

    protected function sql($sql, $parameters = null)
    {
        if ($parameters){
            $queryPrepared = $this->pdo->prepare($sql);
            $queryPrepared->execute($parameters);

            return $queryPrepared;
        }else{
            $queryPrepared = $this->pdo->prepare($sql);
            $queryPrepared->execute();
            return $queryPrepared;
        }
    }
}