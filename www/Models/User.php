<?php

namespace App\Models;

use App\Core\Database;

class User extends Database
{

    private $id = null;
    protected $pseudo;
    protected $email;
    protected $pwd;
    protected $role;
    protected $status;
    protected $isDeleted;
    protected $token;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getDateInserted()
    {
        return $this->date_inserted;
    }

    /**
     * @param mixed $date_inserted
     */
    public function setDateInserted($date_inserted)
    {
        $this->date_inserted = $date_inserted;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $date_updated
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return mixed
     */
    public function getUrlRegister()
    {
        return $this->url_register;
    }

    /**
     * @param mixed $url_register
     */
    public function setUrlRegister($url_register)
    {
        $this->url_register = $url_register;
    }

    /**
     * @return mixed
     */
    public function getTokenRegister()
    {
        return $this->token_register;
    }

    /**
     * @param mixed $token_register
     */
    public function setTokenRegister($token_register)
    {
        $this->token_register = $token_register;
    }
    protected $date_inserted;
    protected $date_updated;
    protected $url_register;
    protected $token_register;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = ucwords(strtolower(trim($pseudo)));
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPwd()
    {
        return $this->pwd;
    }

    /**
     * @param mixed $pwd
     */
    public function setPwd($pwd)
    {
        $this->pwd = $pwd;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    public function formBuilderLogin()
    {
        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "class" => "form_control",
                "id" => "form_register",
                "submit" => "s'inscrire",
            ],
            "inputs" => [
                "email" => [
                    "type" => "email",
                    "placeholder" => "Exemple : nom@gmail.com",
                    "label" => "Votre Email",
                    "required" => true,
                    "class" => "form_input",
                    "minLength" => 8,
                    "maxLength" => 320,
                    "error" => "Votre email doit faire entre 8 et 320 caractères."
                ],
                "pwd" => [
                    "type" => "password",
                    "label" => "Votre mot de passe",
                    "required" => true,
                    "class" => "form_input",
                    "minLength" => 8,
                    "error" => "Votre mot de passe doit faire au moins 8 caractères."
                ]
            ]
        ];
    }

    public function formBuilderRegister()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "class" => "form_control",
                "id" => "form_register",
                "submit" => "s'inscrire",
            ],
            "inputs" => [

                "pseudo" => [
                    "type" => "text",
                    "placeholder" => "ShoyoHinata",
                    "label" => "Votre Pseudo",
                    "required" => true,
                    "class" => "form_input",
                    "minLength" => 2,
                    "maxLength" => 320,
                    "error" => "Votre pseudo doit faire entre 8 et 320 caractères."
                ],

                "email" => [
                    "type" => "email",
                    "placeholder" => "Exemple : nom@gmail.com",
                    "label" => "Votre Email",
                    "required" => true,
                    "class" => "form_input",
                    "minLength" => 8,
                    "maxLength" => 320,
                    "error" => "Votre email doit faire entre 8 et 320 caractères."
                ],

                "pwd" => [
                    "type" => "password",
                    "label" => "Votre mot de passe",
                    "required" => true,
                    "class" => "form_input",
                    "minLength" => 8,
                    "error" => "Votre mot de passe doit faire au moins 8 caractères."
                ],

                "pwdConfirm" => [
                    "type" => "password",
                    "label" => "Confirmation",
                    "required" => true,
                    "class" => "form_input",
                    "confirm" => "pwd",
                    "error" => "Votre mot de passe de confirmation ne correspond pas ."
                ]
            ]
        ];
    }
}